set -xe

mkdir -p public
mkdir -p console
cd proto
i=$DOCKER_RELEASE_IMAGE
t=$DOCKER_TAG_GITREF
unset DOCKER_RELEASE_IMAGE
unset DOCKER_TAG_GITREF
ls
./generate_docs.sh
cd -
DOCKER_RELEASE_IMAGE=$i
DOCKER_TAG_GITREF=$t
mv proto/docs/* console/
