set -xe

cp -r ./console/javascripts public
cp -r ./console/stylesheets public
cp ./console/index.html public/apiconsole.html
