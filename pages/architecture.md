# Objectives



## Server

### Auth
- [ ] Login / Password
- [ ] OAuth2

### Users
- [ ] Add user
- [ ] Edit user
- [ ] Delete user

### Feeds
- [ ] Add feed
- [ ] Delete feed
- [ ] Search feed
- [ ] cron refresh

### Articles
- [ ] Search
- [ ] read/unread
- [ ] favorites

### Deployment
- [ ] continuous deployment
- [ ] Microservice architecture
- [ ] Cloud technology utilization

## Clients

### Web

- [ ] Authentication
- [ ] Add feeds
- [ ] List feeds
- [ ] Delete feeds
- [ ] List feed articles
- [ ] Read/Unread
- [ ] Favorites

### Android

- [ ] Authentication
- [ ] Add feeds
- [ ] List feeds
- [ ] Delete feeds
- [ ] List feed articles
- [ ] Read/Unread
- [ ] Favorites

### Desktop

- [ ] Authentication
- [ ] Add feeds
- [ ] List feeds
- [ ] Delete feeds
- [ ] List feed articles
- [ ] Read/Unread
- [ ] Favorites
