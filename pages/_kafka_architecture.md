
First backend draft, don't hesitate to complete.

!!! todo
    acknowlegement, authentication layer

## Components:

![](/assets/images/backend-architecture.png)


### Scrapper
Will listen to the `refresh` topic, and for each source picked, download
all the contents from a given offset (materialized by an item hash) and pushes
them in the `entries` topic. Start from the latest entry.

### Refresher
Schedule the refreshes.

On startup, import all entries id directly from kafka topics. It then schedule
each id at a random time within the whole refresh window. When an is time expire,
a message is broadcasted on the `refresh` topic with the hash of the topic last message.
It also listen to the `feeds` queue to append at runtime any new feed source, when it
find a new source, it set it's timeout to 0 to perform the source refresh right away.

!!! note
    This component must be a singleton in the application as it is the one who is in charge of letting the system download elements only once. `n` Refreshers in the same system would mean each message is downloaded `n` times.

### Consumer
This component perform queries for content.

This component receive a list of `{ topic: offset }` + a limit `l`.
It aggregate the list of topic id's, create a kafka-stream for each
with the topic offset and take at most `l` elements from each. It then
sort those elements by date and respond the `l` firsts.

!!! note
    This is a bit expensive because we will query `nTopics * l` items to deliver only `l` to the final client.

### API

The api will have 3 roles:
- produce messages on the `feeds` topic for subscribe / unsubscribe
- Read the `feeds/:id` topics
- proxy the contents requests to the `Consumer`

A spec of the api can be browsed
<a
  href="/apiconsole.html"
  target="_blank"> Here</a>
[](__)

## Kafka topics:
- `feeds`
will be sent all the subscribe/unsubscribe events
- `refresh`
Will control the system refresh queue
- `entries`
Will bw sent all the RSS items


- `feeds/:id`
Will store all the feeds history for the user :id
- `entries/:id`
Will store all the entries for the RSS feed :id
