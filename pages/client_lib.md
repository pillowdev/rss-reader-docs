# Client library

This library will implement the server communication logic + the caching for
every application.



let `n` be the number of items we fetch at once

lib workflow:

0. Load local state
1. Get all feeds from the API
2. Download `n` latest items of each feed.


When the user is scrolling:

0. get the date `d` of the current last item
1. sort the feeds by date, and pick `n` before `d`
2. for each feed: if there is less than `n/2` items before `d`, pick `n` more
3. save the results in the local state
