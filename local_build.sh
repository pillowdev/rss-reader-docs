
TARGET=""
BUILD_API="true"

while [[ $# -gt 0 ]]
do
key="$1"

case $key in
    --no-api)
    BUILD_API=""
    shift # past argument
    ;;
    *)    # unknown option
    TARGET=$1
    shift # past argument
    ;;
esac
done


if [ -z "$TARGET" ]
then
  TARGET=$(mktemp -d)
  echo "Processing with temporary directory $TARGET"
fi

set -xe

mkdir -p $TARGET
BUILD_DIR="$( cd $TARGET && pwd )"
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"


mkdocs build -d $BUILD_DIR/public
pushd $BUILD_DIR
  if [ ! -z "$BUILD_API" ]
  then
    $DIR/build_api_console.sh
    $DIR/merge.sh
  fi
  pushd public
    python3 -m http.server
  popd
popd

rm -rf $BUILD_DIR
